ifndef CC
	CC = gcc
endif
# Use this to put auto-generated dependencies for header files
DEPDIR=.depends

# Turn on almost all warnings, but turn off warnigns about no newlines (who the fuck wants that?!)
ifeq ($(CC),clang)
	_WARNFLAGS=-Wno-newline-eof
endif
_WARNFLAGS:=$(_WARNFLAGS) -Wall -Wpedantic -Wextra -Wshadow -Wconversion -Wno-sign-compare
CFLAGS=-std=c99 -Iinclude/ $(_WARNFLAGS)

# Clang doesn't support -Og
ifeq ($(CC),gcc)
	_DBGFLAGS=-ggdb3 -Og
else
	_DBGFLAGS=-ggdb3 -O1
endif
_DBGFLAGS:=$(_DBGFLAGS) -DDEBUG=1
ifeq ($(DEBUG),1)
	CFLAGS:=$(CFLAGS) $(_DBGFLAGS)
endif

LDFLAGS=-lvulkan

DEPFLAGS=-MT $@ -MMD -MP -MF $(DEPDIR)/$*.Td

PRECOMPILE=mkdir -p $(DEPDIR)
POSTCOMPILE=mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d


MAINOBJS=main.o get_dev_info.o util.o

OBJECTS=$(MAINOBJS)
# "Final" targets, ie. things that will be shipped.
FINALTARGETS=main


all: $(FINALTARGETS)
debug:
	@make --no-print-directory all DEBUG=1

main: $(MAINOBJS)
	$(CC) -o main $^ $(LDFLAGS)

# Generic rule for object files
%.o: %.c $(DEPDIR)/%.d
	@$(PRECOMPILE)
	$(CC) $(CFLAGS) \
		$(DEPFLAGS) \
		-c $<
	@$(POSTCOMPILE)

$(DEPDIR)/%.d: ;


run-debug: debug
	@./main
run: main
	@./main

# Clean up recursively, mention files that are deleted. DON'T prompt.
clean:
	@rm -frv $(FINALTARGETS) $(OBJECTS) $(DEPDIR)

# Include our auto-generated dependencies
-include $(patsubst %,$(DEPDIR)/%.d,$(basename $(OBJECTS)))

.PHONY: all clean run-debug
.PRECIOUS: $(DEPDIR)/%.d
