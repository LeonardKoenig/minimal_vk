#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

#include <vulkan/vulkan.h>

#include "util.h"

void print_vk_phys_dev_sparse_props(VkPhysicalDeviceSparseProperties *sparseProps)
{
	LOGV(sparseProps->residencyStandard2DBlockShape, "%u");
	LOGV(sparseProps->residencyStandard2DMultisampleBlockShape, "%u");
	LOGV(sparseProps->residencyStandard3DBlockShape, "%u");
	LOGV(sparseProps->residencyAlignedMipSize, "%u");
	LOGV(sparseProps->residencyNonResidentStrict, "%u");
}

void print_vk_phys_dev_limits(VkPhysicalDeviceLimits *limits)
{
	LOGV(limits->maxImageDimension1D                             , "%u");
	LOGV(limits->maxImageDimension2D                             , "%u");
	LOGV(limits->maxImageDimension3D                             , "%u");
	LOGV(limits->maxImageDimensionCube                           , "%u");
	LOGV(limits->maxImageArrayLayers                             , "%u");
	LOGV(limits->maxTexelBufferElements                          , "%u");
	LOGV(limits->maxUniformBufferRange                           , "%u");
	LOGV(limits->maxStorageBufferRange                           , "%u");
	LOGV(limits->maxPushConstantsSize                            , "%u");
	LOGV(limits->maxMemoryAllocationCount                        , "%u");
	LOGV(limits->maxSamplerAllocationCount                       , "%u");
	LOGV(limits->bufferImageGranularity                          , "%lu");
	LOGV(limits->sparseAddressSpaceSize                          , "%lu");
	LOGV(limits->maxBoundDescriptorSets                          , "%u");
	LOGV(limits->maxPerStageDescriptorSamplers                   , "%u");
	LOGV(limits->maxPerStageDescriptorUniformBuffers             , "%u");
	LOGV(limits->maxPerStageDescriptorStorageBuffers             , "%u");
	LOGV(limits->maxPerStageDescriptorSampledImages              , "%u");
	LOGV(limits->maxPerStageDescriptorStorageImages              , "%u");
	LOGV(limits->maxPerStageDescriptorInputAttachments           , "%u");
	LOGV(limits->maxPerStageResources                            , "%u");
	LOGV(limits->maxDescriptorSetSamplers                        , "%u");
	LOGV(limits->maxDescriptorSetUniformBuffers                  , "%u");
	LOGV(limits->maxDescriptorSetUniformBuffersDynamic           , "%u");
	LOGV(limits->maxDescriptorSetStorageBuffers                  , "%u");
	LOGV(limits->maxDescriptorSetStorageBuffersDynamic           , "%u");
	LOGV(limits->maxDescriptorSetSampledImages                   , "%u");
	LOGV(limits->maxDescriptorSetStorageImages                   , "%u");
	LOGV(limits->maxDescriptorSetInputAttachments                , "%u");
	LOGV(limits->maxVertexInputAttributes                        , "%u");
	LOGV(limits->maxVertexInputBindings                          , "%u");
	LOGV(limits->maxVertexInputAttributeOffset                   , "%u");
	LOGV(limits->maxVertexInputBindingStride                     , "%u");
	LOGV(limits->maxVertexOutputComponents                       , "%u");
	LOGV(limits->maxTessellationGenerationLevel                  , "%u");
	LOGV(limits->maxTessellationPatchSize                        , "%u");
	LOGV(limits->maxTessellationControlPerVertexInputComponents  , "%u");
	LOGV(limits->maxTessellationControlPerVertexOutputComponents , "%u");
	LOGV(limits->maxTessellationControlPerPatchOutputComponents  , "%u");
	LOGV(limits->maxTessellationControlTotalOutputComponents     , "%u");
	LOGV(limits->maxTessellationEvaluationInputComponents        , "%u");
	LOGV(limits->maxTessellationEvaluationOutputComponents       , "%u");
	LOGV(limits->maxGeometryShaderInvocations                    , "%u");
	LOGV(limits->maxGeometryInputComponents                      , "%u");
	LOGV(limits->maxGeometryOutputComponents                     , "%u");
	LOGV(limits->maxGeometryOutputVertices                       , "%u");
	LOGV(limits->maxGeometryTotalOutputComponents                , "%u");
	LOGV(limits->maxFragmentInputComponents                      , "%u");
	LOGV(limits->maxFragmentOutputAttachments                    , "%u");
	LOGV(limits->maxFragmentDualSrcAttachments                   , "%u");
	LOGV(limits->maxFragmentCombinedOutputResources              , "%u");
	LOGV(limits->maxComputeSharedMemorySize                      , "%u");
	for (int i = 0; i < 3; i++) {
		LOGV(limits->maxComputeWorkGroupCount[i]             , "%u");
	}
	LOGV(limits->maxComputeWorkGroupInvocations                  , "%u");
	for (int i = 0; i < 3; i++) {
		LOGV(limits->maxComputeWorkGroupSize[i]              , "%u");
	}
	LOGV(limits->subPixelPrecisionBits                           , "%u");
	LOGV(limits->subTexelPrecisionBits                           , "%u");
	LOGV(limits->mipmapPrecisionBits                             , "%u");
	LOGV(limits->maxDrawIndexedIndexValue                        , "%u");
	LOGV(limits->maxDrawIndirectCount                            , "%u");
	LOGV(limits->maxSamplerLodBias                               , "%f");
	LOGV(limits->maxSamplerAnisotropy                            , "%f");
	LOGV(limits->maxViewports                                    , "%u");
	for (int i = 0; i < 2; i++) {
		LOGV(limits->maxViewportDimensions[i]                , "%u");
	}
	for (int i = 0; i < 2; i++) {
		LOGV(limits->viewportBoundsRange[i]                  , "%f");
	}
	LOGV(limits->viewportSubPixelBits                            , "%u");
	LOGV(limits->minMemoryMapAlignment                           , "%zu");
	LOGV(limits->minTexelBufferOffsetAlignment                   , "%lu");
	LOGV(limits->minUniformBufferOffsetAlignment                 , "%lu");
	LOGV(limits->minStorageBufferOffsetAlignment                 , "%lu");
	LOGV(limits->minTexelOffset                                  , "%d");
	LOGV(limits->maxTexelOffset                                  , "%u");
	LOGV(limits->minTexelGatherOffset                            , "%d");
	LOGV(limits->maxTexelGatherOffset                            , "%u");
	LOGV(limits->minInterpolationOffset                          , "%f");
	LOGV(limits->maxInterpolationOffset                          , "%f");
	LOGV(limits->subPixelInterpolationOffsetBits                 , "%u");
	LOGV(limits->maxFramebufferWidth                             , "%u");
	LOGV(limits->maxFramebufferHeight                            , "%u");
	LOGV(limits->maxFramebufferLayers                            , "%u");
	LOGV(limits->framebufferColorSampleCounts                    , "%u");
	LOGV(limits->framebufferDepthSampleCounts                    , "%u");
	LOGV(limits->framebufferStencilSampleCounts                  , "%u");
	LOGV(limits->framebufferNoAttachmentsSampleCounts            , "%u");
	LOGV(limits->maxColorAttachments                             , "%u");
	LOGV(limits->sampledImageColorSampleCounts                   , "%u");
	LOGV(limits->sampledImageIntegerSampleCounts                 , "%u");
	LOGV(limits->sampledImageDepthSampleCounts                   , "%u");
	LOGV(limits->sampledImageStencilSampleCounts                 , "%u");
	LOGV(limits->storageImageSampleCounts                        , "%u");
	LOGV(limits->maxSampleMaskWords                              , "%u");
	LOGV(limits->timestampComputeAndGraphics                     , "%u");
	LOGV(limits->timestampPeriod                                 , "%f");
	LOGV(limits->maxClipDistances                                , "%u");
	LOGV(limits->maxCullDistances                                , "%u");
	LOGV(limits->maxCombinedClipAndCullDistances                 , "%u");
	LOGV(limits->discreteQueuePriorities                         , "%u");
	for (int i = 0; i < 2; i++) {
		LOGV(limits->pointSizeRange[i]                       , "%f");
	}
	for (int i = 0; i < 2; i++) {
		LOGV(limits->lineWidthRange[i]                       , "%f");
	}
	LOGV(limits->pointSizeGranularity                            , "%f");
	LOGV(limits->lineWidthGranularity                            , "%f");
	LOGV(limits->strictLines                                     , "%u");
	LOGV(limits->standardSampleLocations                         , "%u");
	LOGV(limits->optimalBufferCopyOffsetAlignment                , "%lu");
	LOGV(limits->optimalBufferCopyRowPitchAlignment              , "%lu");
	LOGV(limits->nonCoherentAtomSize                             , "%lu");
}

void print_phys_dev_api_version(uint32_t apiVersion)
{
	/* 31 - 22 (upper 10) bits are for major version
	 * 21 - 12 (mid 10) bits are for minor version
	 * 11 - 0 (lower 12) bits are for patch version
	 */
	uint32_t MAJ_BM =	0xFFC00000;
	uint32_t MIN_BM =	0x003FF000;
	uint32_t PATCH_BM =	0xFFF;
	uint32_t maj_ver = apiVersion & MAJ_BM;
	maj_ver = maj_ver >> 22;
	uint32_t min_ver = apiVersion & MIN_BM;
	min_ver = min_ver >> 11;
	uint32_t patch_ver = apiVersion & PATCH_BM;
	LOGV(maj_ver, "%u");
	LOGV(min_ver, "%u");
	LOGV(patch_ver, "%u");
}

void print_vk_phys_dev_type(VkPhysicalDeviceType type)
{
	switch(type) {
	case VK_PHYSICAL_DEVICE_TYPE_OTHER:
		LOG(INF, "VkPhysicalDeviceType = %s\n",
				"VK_PHYSICAL_DEVICE_TYPE_OTHER");
		break;
	case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
		LOG(INF, "VkPhysicalDeviceType = %s\n",
				"VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU");
		break;
	case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
		LOG(INF, "VkPhysicalDeviceType = %s\n",
				"VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU");
		break;
	case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
		LOG(INF, "VkPhysicalDeviceType = %s\n",
				"VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU");
		break;
	case VK_PHYSICAL_DEVICE_TYPE_CPU:
		LOG(INF, "VkPhysicalDeviceType = %s\n",
				"VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU");
		break;
	default:
		LOG(WRN, "%s", "Unkown VkPhysicalDeviceType!\n");
		break;
	}
}

void print_vk_phys_dev_props(VkPhysicalDeviceProperties *pProperties)
{
	LOG(INF, "pProperties->apiVersion = %u:\n", pProperties->apiVersion);
	print_phys_dev_api_version(pProperties->apiVersion);
	LOGV(pProperties->driverVersion, "%u");
	LOGV(pProperties->vendorID, "%u");
	LOGV(pProperties->deviceID, "%u");
	LOG(INF, "%s", "pProperties->deviceType:\n");
	print_vk_phys_dev_type(pProperties->deviceType);
	LOGV(pProperties->deviceName, "%s");
	for (uint32_t i = 0; i < VK_UUID_SIZE; i++) {
		LOGV(pProperties->pipelineCacheUUID[i], "%u");
	}
	LOG(INF, "%s", "pProperties->limits:\n");
	print_vk_phys_dev_limits(&pProperties->limits);
	LOG(INF, "%s", "pProperties->sparseProperties:\n");
	print_vk_phys_dev_sparse_props(&pProperties->sparseProperties);
}

void print_dev_info(VkInstance vk_instance)
{
	VkResult res;

	uint32_t vk_phys_dev_count;

	/* gets us the number of devices available */
	res = vkEnumeratePhysicalDevices(vk_instance, &vk_phys_dev_count,
			NULL);

	if (res != VK_SUCCESS) {
		fprintf(stderr, "vkEnumeratePhysicalDevices not VK_SUCCESS: %d\n",
				VK_SUCCESS);
		return;
	}

	LOGV(vk_phys_dev_count, "%u");

	VkPhysicalDevice vk_phys_devs[vk_phys_dev_count];
	res = vkEnumeratePhysicalDevices(vk_instance, &vk_phys_dev_count,
			vk_phys_devs);

	if (res != VK_SUCCESS) {
		fprintf(stderr, "vkEnumeratePhysicalDevices not VK_SUCCESS: %d\n",
				VK_SUCCESS);
		return;
	}

	VkPhysicalDeviceProperties vk_phys_dev_props[vk_phys_dev_count];
	for (uint32_t i = 0; i < vk_phys_dev_count; i++) {
		vkGetPhysicalDeviceProperties(vk_phys_devs[i],
				&vk_phys_dev_props[i]);
		print_vk_phys_dev_props(&vk_phys_dev_props[i]);
	}
}
