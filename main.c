#include <stdlib.h>
#include <stdio.h>

#include <vulkan/vulkan.h>

#include "util.h"
#include "get_dev_info.h"

int main()
{
	VkResult res;

	VkApplicationInfo vk_app_info = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pNext = NULL, /* no extensions in use */
		.pApplicationName = "minimal_vk",
		.applicationVersion = 1,
		.pEngineName = NULL, /* we don't use an engine */
		.engineVersion = 0,
		.apiVersion = 0 /* ignore specific API rev */
	};

	VkInstanceCreateInfo vk_instance_info = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pNext = NULL, /* no extensions in use */
		.flags = 0, /* reserved for future use */
		.pApplicationInfo = &vk_app_info,
		/* we don't want anything here for now */
		.enabledLayerCount = 0,
		.ppEnabledLayerNames = NULL,
		.enabledExtensionCount = 0,
		.ppEnabledExtensionNames = NULL
	};

	VkInstance vk_instance;
	res = vkCreateInstance(&vk_instance_info, NULL, &vk_instance);

	if (res != VK_SUCCESS) {
		LOG(ERR, "vkCreateInstance not VK_SUCCESS: %s\n", vk_err(res));
		return 1;
	}

	/**
	 * Do stuff in here
	 */
	//print_dev_info(vk_instance);

	/* for now just get us the first / only device */
	VkPhysicalDevice vk_phys_devs[1];
	uint32_t count = 1;
	res = vkEnumeratePhysicalDevices(vk_instance, &count, vk_phys_devs);

	if (res != VK_SUCCESS) {
		LOG(ERR, "vkEnumeratePhysicalDevices: %s\n", vk_err(res));
		return 1;
	}

	float q_props[] = { 0.0 };
	/* now get us our "virtual" VkDevice */
	VkDeviceQueueCreateInfo vk_dev_q_create_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queueFamilyIndex = 0,
		.queueCount = 1,
		.pQueuePriorities = q_props
	};

	VkDeviceCreateInfo vk_dev_create_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queueCreateInfoCount = 1,
		.pQueueCreateInfos = &vk_dev_q_create_info,
		.enabledLayerCount = 0,
		.ppEnabledLayerNames = NULL,
		.enabledExtensionCount = 0,
		.ppEnabledExtensionNames = NULL,
		.pEnabledFeatures = NULL
	};

	VkDevice vk_dev;
	res = vkCreateDevice(vk_phys_devs[0], &vk_dev_create_info, NULL,
			&vk_dev);
	if (res != VK_SUCCESS) {
		LOG(ERR, "vkCreateDevice: %s\n", vk_err(res));
		return 1;
	}

	vkDestroyInstance(vk_instance, NULL);
}
