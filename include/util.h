#ifndef _VKMIN_UTIL_H
#define _VKMIN_UTIL_H

#include <stdio.h>

#include <vulkan/vulkan.h>

/* Usage:
 * LOG(ERR, "something nasty happened, i is: %d\n", 42);
 * Prints:
 * "[ERR] something nasty happened, i is: 42"
 *
 * Ie.: Almost same syntax as fprintf/printf.
 *
 * @param val should be in:
 *  - DBG
 *  - INF
 *  - WRN
 *  - ERR
 */
#define LOG(val, fmt, ... ); fprintf(stderr, "[" #val "] " fmt, __VA_ARGS__);

/* Usage:
 * some_integer = 42;
 * LOGV(some_integer, "%d");
 * Prints:
 * "[INF] some_integer = 42"
 *
 * Shorthand for printing to console.
 */
#define LOGV(val, fmt); LOG(INF, #val " = " fmt "\n", val);

char *vk_err(VkResult);

#endif
